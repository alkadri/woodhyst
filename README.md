Wood-Hyst
================

This is an R package containing many functions used for working with
modelling the moisture adsorption-desorption curves o This package is
licensed under MIT License. The current main contributors, direct and
indirect, of this package are:

  - Ahmad Alkadri (<ahmad.alkadri@umontpellier.fr>)
  - Olivier Arnould
  - Delphine Jullien
  - Louise Hovasse
  - Joseph Gril

This package is constantly under development for now. We’re still
actively working with the Terahertz technique. Active developments and
updates will be made regularly. Any further questions should be directed
by e-mail to <ahmad.alkadri@umontpellier.fr>.

## How to install

For installing the package on your computer, simply open your R (via
console or IDE such as RStudio) and type:

    devtools::install_git("https://git-xen.lmgc.univ-montp2.fr/alkadri/woodhyst.git")

Don’t forget, you need to have *devtools*, *git2r*, and *getPass*
packages installed on your R in your computer in order to run that code.

## Dependencies

This package depends on the following R packages: *rio*. If you don’t
have them yet, don’t worry, once you run the above code to install this
package, those packages will be installed too.
